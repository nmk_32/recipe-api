# Recipe API

Recipe API acts as a proxy for Food2fork API. This proxy is built on Mule 4.1.5 and make use of rest calls to communicate to food2fork api.

API contains two endpoints

/search - For searching recipes with a query string
	Sample End Point URL : http://food2fork.us-e2.cloudhub.io/api/search?query=pasta
	Query parameters:
	query : pasta
		client_id : xxxxx
		client_secret : xxxx

/search/{recipeId} - For searching recipe with an ID
	Sample End Point URL : http://food2fork.us-e2.cloudhub.io/api/search/35120
	URI parameters :
	recipeID : 35120
	Query parameters:
		client_id : xxxxx
		client_secret : xxxx


## Steps to run

```
To run this application you need to give the environment variable 
and key to decrypt the encrypted Food2Fork API Key
#Sample runtime configuration 
-Denv=dev -Dkey = food2fork

```

## Use Cases

1. For /search API accepts string with a comma separated values which do not contains "egg" word in them.

2. For /search/{recipeID} endpoint accepts a recipe ID as a URI parameter and gives you recipe details if doesn't contain egg as an ingredient.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
